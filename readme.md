Program I coded in 2019. It takes in a text file with a 2D array of customers and the floors they want to go to, and then simulates their movement between floors in a shopping centre with 2 lifts. You can run it with your own text file, by changing the 'path' string in Runnable.java — look at 
the examples (eg shop_12.txt) for how they're structured.

The program runs in realtime, with 2-second increments between each update. Customers stay on each floor for 10 seconds, then wait to be picked up by the lift to go to their next location.

Customers in a lift vote on whether it should go up or down, and the lift goes wherever the majority want it to.

Lifts can sometimes get stuck in an infinite loop, perhaps a side effect of the realtime nature of the program. This is something I managed to reduce a lot, and had further plans to, but then decided other projects were more worth my time.

![image of shopping centre program running in console](example_of_program_running.png "Image of program running")
