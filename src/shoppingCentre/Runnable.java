package shoppingCentre;

import java.util.regex.Pattern;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Runnable {

	public static void main (String [] args) throws FileNotFoundException {

		// Read from file, sort input into sensible structures.
		String path = "./shop_12.txt";
		Scanner scn = new Scanner(new File(path));
		String file_input = scn.nextLine();
		scn.close();	// close the Scanner.

		// fiddly text operations to arrange Customers from file into array, where they are then transferred into an ArrayList
		String[] info = file_input.split(Pattern.quote("], [["));
		String names = info[0];
		String floorsToGo = info[1];
		names = names.substring(2, names.length());
		String[] name_list = names.substring(1,names.length()-1).split("', '");
		// names done as array name_list

		floorsToGo = "[" + floorsToGo;
		String[] floors = floorsToGo.substring(1, floorsToGo.length()-3).split(Pattern.quote("], ["));
		int[][] destinations = new int[name_list.length][5];
		for(int f = 0; f < floors.length; f++) {
			String[] floorNums = floors[f].split(", ");
			int[] d = new int[5];
			for(int i = 0; i<5; i++) {
				if(i >= floorNums.length) {
					d[i] = 0;
				} else {
					d[i] = Integer.parseInt(floorNums[i]);
				}
			}
			destinations[f] = d;
		}

		for(int i = 0; i < name_list.length; i++) {
			// actually put Customers in ArrayList
			Shopping_Centre.finalFloors[0].getLiftQueue().add((new Customer(name_list[i], destinations[i])));
 		}

		final Lift a = new Lift('a');
		final Lift b = new Lift('b');

		Timer timer = new Timer();

		System.out.println("~~~~~~~~~~  Shopping Centre, author Zogalicious!  ~~~~~~~~~~\n\nLift is on the Ground Floor, and is ready to take Customers up!\n");
		a.receiveCustomers(0); // FIXED (this works, but so far when referring to array it doesn't work)
		b.receiveCustomers(0);

		timer.scheduleAtFixedRate ( new TimerTask() {
			public void run() {

				/*
				 * travel(int direction, Floor[] floors)
				 *   find(Floor[] floors, int[] aboveAndBelow)
				 *     vote()
				 *
				 * find() returns direction for Lift to go in
				 *   vote() returns aboveAndBelow
				 */

				a.travel(a.find(a.occupantsVote()));
				b.travel(b.find(b.occupantsVote()));

				System.out.println();

				if(Shopping_Centre.CUSTOMERS == 0) {
					System.out.println("\n\n\t\t~~~~~ Supermarket closing for today! ~~~~~");
					System.exit(0);
				}
			}
		}, 0, 2000);
	}
}
