package shoppingCentre;

import shoppingCentre.Customer;

import java.util.ArrayList;

public class Customer {
	private ActionListener actionListener;
	private String name;
	private ArrayList<Integer> floorsToGo;
	//private int currFloor;
	
	public Customer(String name, int[] floorsToGo) {
		Shopping_Centre.CUSTOMERS++;
		this.name = name;
		this.floorsToGo = new ArrayList<>();
		for(int i = 0; i < floorsToGo.length; i++) {
			if(floorsToGo[i] != 0) { // this is because we padded out the array in Runnable with zeroes, so now we need to remove them
				// take away 1 from all Floors Customers want to go to, as I am using 0 as Ground Floor and increasing numbers for concurrent Floors, up to Floor 7
				this.floorsToGo.add(floorsToGo[i]-1);
			}
		}
		this.floorsToGo.add(0); // Customer needs to leave Supermarket at Ground Floor (Floor 0)
		actionListener = new ActionListener();
	}

	public String getName() {
		return name;
	}

	public int getNextFloor() {
		return floorsToGo.get(0);
	}
	
	public ActionListener getActionListener() {
		return actionListener;
	}
	
	public void removeEnteredFloor() {
		floorsToGo.remove(0);
		// currFloor is updated in travel(), in Lift
	}
}