package shoppingCentre;

import java.util.Timer;
import java.util.TimerTask;

public class ActionListener {
	
	private int count;

	public ActionListener() {
		count = 0;
	}
	
	public void enterFloor(Customer c, int floorNum, int i) {
		/* get index of Floor Object in finalFloors array from passed in Floor number
		 * because index has to be final as I'm working with a Timer, I can only initialise it once
		 * This means I figure out what the index is, put it in a variable, and then pass it into the final variable
		 * fFI stands for Final Floors Index
		 * fFFI stands for final Final Floors Index!
		 */
		
		int fFI = 0;
		for(int j = 0; j < Shopping_Centre.finalFloors.length; j++) {
			if(Shopping_Centre.finalFloors[j].getFloorNumber() == floorNum) {
				fFI = j;
			}
		}
		final int fFFI = fFI;
		
		//System.out.println("Entering method enterFloor()!");
		int delay = 0; // delay for 0 sec.
		int period = 2000; // repeat every 2 secs.
		
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() { 
			public void run() { 
				if(++count == 5) {
					timer.cancel();
					timer.purge();
					count = 0;
	            	Shopping_Centre.finalFloors[fFFI].getLiftQueue().add(c);
	            	System.out.println();
	            		
	            	if(c.getNextFloor() == 0) {
	            		System.out.println("\n" + (i+1) + ": Customer " + c.getName() + " has entered Floor " + Shopping_Centre.finalFloors[fFFI].getFloorNumber() + "'s Lift Queue,\n" + (i+1)
	            				+ ": and now wants to leave the Supermarket\n");
		            }
		            else {
		            	System.out.println("\n" + (i+1) + ": Customer " + c.getName() + " has entered Floor " + Shopping_Centre.finalFloors[fFFI].getFloorNumber() + "'s Lift Queue,\n" + (i+1)
		            			+ ": and is next going to Floor " + c.getNextFloor() + "\n");
		            }
		               	return;
		        }
	        }
	    }, delay, period);
	}
}
