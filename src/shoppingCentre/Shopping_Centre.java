/* if I was to recreate this, I would just use a Floor object in Lift instead of having
 * to convert between Floors and ints, as that caused many previous bugs
 * 
 * I also might use slightly more descriptive names, although later on
 * I changed most of them to be more descriptive
 * 
 * And I would comment a lot more from the start, as most the comments
 * I've written in quite recently
 */

/*
 * What this program does:
 * 
 *  	 it is a supermarket with 8 floors (incl. Ground Floor)
 *  
 *  	 it reads in customers and what floors they want to go to from a file
 *   (shop_12.txt or shop_30.txt or shop_50.txt*, although other files could be created for it)
 *  
 *   there are two lifts ('a' and 'b'), which take the customers to their desired floors,
 *   and out the supermarket after they've been to all their floors
 *   
 *   it prints out to the Console whenever a customer does/wants to do
 *   anything (entering/leaving Floors, wants to leave supermarket now, etc.),
 *   and whenever a lift goes to a new floor or picks up customers from a floor
 *  
 *   it uses Timers. Lifts travel between each floor in intervals of 2 secs, and
 *   customers stay 10 secs on each floor before wanting to leave that floor
 *  
 *  current bugs:
 *  
 *   it is still buggy, and doesn't work on shop_50.txt*, but most of the time it works
 *   on the other files
 *  
 *   *it doesn't work on shop_50.txt because of a complicated bug
 *   (I think I have now figured out why it was occurring) where the lifts got in a sort of
 *   infinite loop between floors...
 *  
 *   ...in fact, most of the bugs are occurring because of some sort or other of infinite loop
 *   between floors the lift is going to, the lifts are staying on the same floor infinitely,
 *   or the rare NullPointerException,
 *   just I'm not sure why they are happening (apart from maybe the shop_50.txt one)
 *   
 */

package shoppingCentre;

public class Shopping_Centre {
	public static int CUSTOMERS; // No. of Customers in Shopping Centre
	
	// initialise GLOBAL (not local, as that caused many bugs!) floors
	public static Floor ground_floor = new Floor(0);
	public static Floor floor1 = new Floor(1);
	public static Floor floor2 = new Floor(2);
	public static Floor floor3 = new Floor(3);
	public static Floor floor4 = new Floor(4);
	public static Floor floor5 = new Floor(5);
	public static Floor floor6 = new Floor(6);
	public static Floor floor7 = new Floor(7);
	
	// put them all in an array
	public static Floor[] finalFloors = {ground_floor, floor1, floor2, floor3, floor4, floor5, floor6, floor7};
}