package shoppingCentre;

import java.util.ArrayList;

public class Floor {
	private ArrayList<Customer> customersVisiting;
	//private CustomerQueue liftQueue;
	private ArrayList<Customer> liftQueue;
	private int floorNumber;
	
	public int getHeadcount() {
		return customersVisiting.size();
	}

	public Floor(int floorNumber) {
		customersVisiting = new ArrayList<Customer>();
		liftQueue = new ArrayList<Customer>();
		this.floorNumber = floorNumber;
	}

	public int getFloorNumber() {
		return floorNumber;
	}

	public void setFloorNumber(int floorNumber) {
		this.floorNumber = floorNumber;
	}

	public void addCustomer(Customer c) {
		customersVisiting.add(c);
	}
	
	public ArrayList<Customer> getCustomersVisiting() {
		return customersVisiting;
	}

	public void setCustomersVisiting(ArrayList<Customer> customersVisiting) {
		this.customersVisiting = customersVisiting;
	}

	public ArrayList<Customer> getLiftQueue() {
		return liftQueue;
	}

	public ArrayList<Customer> getNumberVisiting() {
		return customersVisiting;
	}

	public void setNumberVisiting(ArrayList<Customer> customersVisiting) {
		this.customersVisiting = customersVisiting;
	}

	public void setLiftQueue(ArrayList<Customer> liftQueue) {
		this.liftQueue = liftQueue;
	}
}