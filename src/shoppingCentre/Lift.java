package shoppingCentre;

import java.util.Random;

public class Lift {
	/*
	 * in shop_50.txt, bug in only switching between Floors 2, 3, and 4 at some point every time. TODO: fix: To stop this, use 
	 * flags to cache previous Floors gone to, and if there is any repetition, go to different Floors (convert to HashSet and 
	 * compare to ArrayList?) (not sure how to decide what different Floors to go to yet). Probably happens because there is 
	 * infinite loop in where Customers want to go to, but in which no Customers actually want to get off at Floors Lift is 
	 * going to in infinite loop. Probably change fullOccupantsVote().
	 */
	
	/* FIXED: IMPORTANT!
	 * 
	 * 9: Customer Christine Parker has entered Floor 3's Lift Queue,
		9: and is next going to Floor 7
		
		f.getLiftQueue().get(i).getName() on Floor 3 is Christine Parker
		
		6: Customer Angelica Pena has entered Floor 3's Lift Queue,
		6: and is next going to Floor 5
		
		f.getLiftQueue().get(i).getName() on Floor 3 is Christine Parker
	 */
	
	// I think there is could b a problem with Customer entering Lift!
	// ...previous me, you were correct, but I accidentally fixed it now :). Also brush up on grammar
	
	private char name;
	private int currFloor;
	private Customer[] occupants;
	// when -1, doesn't mean anything, but when set to Floor value, it does
	private int floorToGoTo = -1;
	
	public Lift(char name) {
		this.name = name;
		occupants = new Customer[10];
		currFloor = 0; // set current floor to Ground Floor
	}

	public Customer[] getOccupants() {
		return occupants;
	}
	
	// Test method
	public void addOccupants() {
		Random rand = new Random();
		
		int[] c1Floors = new int[5];

		// Randomises floorsToGo, with there being 5 floors to go to
		for(int i = 0; i < 5; i++) {
			c1Floors[i] = rand.nextInt(7)+2;
		}
		
		// Assigns floorsToGo to some customers
		Customer c1 = new Customer("Jimmy", c1Floors);
		
		// Assigns Customers to occupants
		occupants[0] = c1;

		System.out.println("Jimmy is going to " + (c1Floors[0]-1));
	}
	
	public void printOccupants() {
		String aPositions = "";
		for(int i = 0; i < occupants.length; i++) {
			if(occupants[i] != null) {
				aPositions += occupants[i].getName() + ", ";
			}
			else {
				aPositions += "null, ";
			}
		}
		System.out.println("\nLift a positions: " + aPositions);

	}
	
	// Travels in direction specified by vote(). Returns floor travelled to.
	// Lift is sometimes going in the opposite direction that Jimmy wants after picking him up from Lift Queue TODO: fix
	public void travel(int direction) {
		
		// receive Customers on current Floor (if Lift is full or Lift Queue empty, method deals with that)
		receiveCustomers(currFloor);
		
		/*
		 * direction codes:
		 * 0 == down
		 * 1 == up
		 * 2 == stay
		 * FIXED (before finishing bug comment!):For some reason, in new bug not entering any of these if... else if... else blocks in certain scenario. Not even entering else block, meaning 
		 */
		
		// if travelling up
		if(direction == 1) {
			System.out.println("\nLift " + name + " is travelling to Floor " + (currFloor+1));
			currFloor++;
		}
		// if travelling down
		else if(direction == 0) {
			// if travelling to Ground Floor
			if(currFloor-1 == 0) {
				System.out.println("\nLift " + name + " is travelling to Ground Floor");
			}
			else {
				System.out.println("\nLift " + name + " is travelling to Floor " + (currFloor-1));
			}
			currFloor--;
		}
		// if staying on same floor
		else if(direction == 2) {
			if(fullOccupantsVote() == 2) {
				if(currFloor == 0) {
					// if staying on Ground Floor
					System.out.println("\nLift " + name + " is staying on Ground Floor to wait for a/some Customer(s)");
				}
				else {
					System.out.println("\nLift " + name + " is staying on Floor " + (currFloor) + " to wait for a/some Customer(s)");
				}
				return;
			}
			else if(fullOccupantsVote() == 1) {
				currFloor++;
				if(empty()) {
					return;
				}
			}
			
			else if(fullOccupantsVote() == 0) {
				currFloor--;
				if(empty()) {
					return;
				}
			}
		}
		
		else {
			System.err.println("ERROR IN LIFT: LINE 135");
		}
		
		for(int i = 0; i < occupants.length; i++) {
			// if occupants[i] does not contain a Customer, iterate to next occupant
			if(occupants[i] == null) {
				continue;
			}
			// if next Floor Customer wants to go to is current Lift Floor (but not exiting Supermarket on Ground Floor, managed in else block), exit Lift onto Floor
			if(occupants[i].getNextFloor() == currFloor && occupants[i].getNextFloor() != 0) {
				// removes Floor Customer is entering from floorsToGo
				occupants[i].removeEnteredFloor();
				// start timer
				occupants[i].getActionListener().enterFloor(occupants[i], currFloor, i);
 				
 				// says which Floor occupants[i] is now visiting
				System.out.println("\n" + (i+1) + ": Customer " + occupants[i].getName() + " is visiting Floor " + (currFloor) + "\n");
				// remove occupants[i] from Lift occupants
				occupants[i] = null;			
				break;	
			}
			
			// if occupant does not want to get off on current Floor
			else if(occupants[i].getNextFloor() != currFloor) {
				if(currFloor != 0) {
					System.out.println(i+1 + ": The Lift took " + occupants[i].getName() + " to Floor " + (currFloor));
				}
				// if on Ground Floor
				else {
					System.out.println(i+1 + ": The Lift took " + occupants[i].getName() + " to Ground Floor");
				}
			}
			
			// if occupant wants to exit Supermarket
			else {
				System.out.println(i+1 + ": " + occupants[i].getName() + " has left the Supermarket (on Ground Floor)");
				// remove occupants[i] from Lift occupants
				occupants[i] = null;
				Shopping_Centre.CUSTOMERS--;
			}
		}
	}

	// Lift decides to go up or down every time a new customer joins (used to be just vote())
	public int[] occupantsVote() {
		int[] aboveAndBelow = {0, 0};

		for(int i = 0; i < occupants.length; i++) {
			if(occupants[i] != null) {
				if(occupants[i].getNextFloor() > currFloor) { // vote up
					aboveAndBelow[0]++;
				}
				
				else {
					aboveAndBelow[1]++;
				}
			}
		}
		
		return aboveAndBelow;
	}
	
	public int fullOccupantsVote() {
		int occupantsNo = 0;
		int up = 0;
		int down = 0;
		
		if(empty()) {
			return 2;
		}
		
		for(int i = 0; i < occupants.length; i++) {
			if(occupants[i] != null) {
				if(occupants[i].getNextFloor() > currFloor) { // Vote up
					up++;
				}
			}
		}
		
		for(int i = 0; i < occupants.length; i++) {
			if(occupants[i] != null) {
				if(occupants[i].getNextFloor() < currFloor) { // Vote up
					down++;
				}
			}
		}
		
		// return key to go up
		if(up > down) {
			return 1;
		}
		
		// return key to go down
		else if(up < down) {
			return 0;
		}
		
		// return key for Lift to stay where it is
		else if(up == down) {
			return 3;
		}
		
		// return error code
		else {
			System.err.println("occupantsNo = " + occupantsNo + ". Error with fullOccupantsVote()!");
			return 8;
		}
	}

	public int find(int[] aboveAndBelow) {
		// if lift is full...
		if(free() == false) {
		// call fullOccupantsVote() (which only takes into account where occupants want to go) in some if statements
			// if occupants want to go up...
			if(fullOccupantsVote() == 1) {
				// return up direction code
				return 1;
			}
			// if vote is split 5v5 to go up or down...
			else if(fullOccupantsVote() == 3) {
				// move on to next part of find() to see whether there are the most Customers above or below the Lift
			}
			
			// if occupants want to go down...
			else {
				// return down direction code
				return 0;
			}
		}
		int pivot = 0;
		
		/* from the for loop, the code is saying:
		 * if no one is in any lift queues, stay on current floor
		 * 
		 * problem: if no one is in any lift queue, Lift will
		 */
		
		// loop through floors
		for(int l = 0; l < Shopping_Centre.finalFloors.length; l++) {
			// if no one is in lift queue in specific Floor iteration
			if(Shopping_Centre.finalFloors[l].getLiftQueue().size() == 0) {
				// increment pivot per empty floor, and if it == 8, return code for Lift to stay where it is
				if(++pivot == 8) {
					return 2;
				}
			}
		}
		int above = 0;
		int below = 0;
		
		for(int j = 0; j < currFloor; j++) {
			for(int k = 0; k < Shopping_Centre.finalFloors[j].getLiftQueue().size(); k++) {
				if(Shopping_Centre.finalFloors[j].getLiftQueue().get(k) != null) {
					below++;
				}
			}
		}
		for(int i = Shopping_Centre.finalFloors.length-1; i > currFloor; i--) {
			for(int c = 0; c < Shopping_Centre.finalFloors[i].getLiftQueue().size(); c++) {
				if(Shopping_Centre.finalFloors[i].getLiftQueue().get(c) != null) {
					above++;
				}
			}
		}
		above += aboveAndBelow[0];
		below += aboveAndBelow[1];
		
		// return up code
		if(above > below) {
			return 1;
		}
		
		// return down code
		else if(above < below) {
			return 0;
		}
		
		// return random code, as votes are equal
		else {
			Random rand = new Random();
			return rand.nextInt(2);
		}
	}
	
	// Checks if lift is free; i.e. < 10 customers
	public boolean free() {
		for(int i = 0; i < occupants.length; i++) {
			if(occupants[i] == null) {
				return true;
			}
		}
		return false;
	}
	
	public boolean empty() {
		for(int i = 0; i < occupants.length; i++) {
			if(occupants[i] != null) {
				return false;
			}
		}
		return true;
	}
	
	/* FIXED: (NOT TAKING CUSTOMERS OUT OF FLOOR'S LIFT QUEUE – local/global variable issue; currFloorObject is only updating locally, not updating actual variable in Runnable – to fix, make global)
	 *
	 * (Customer Lucy Rodriquez has entered Lift a on Floor 4
	 * ...then...
	 * There is Customer Lucy Rodriquez on Floor 4's Lift Queue)
	 * 
	 * NOW THIS ALSO FIXED (somehow when I reformatted travel(), without knowingly doing anything influential to the code):
	 * 
	 * But now NullPointerException being thrown. It is the getName() part of the expression on line 412 that throws it
	 * Even though the get() part is null, Exception is only thrown with getName(), not get(). Strange.
	 * Handled it with try/catch blocks before, but now I know it didn't work as it just meant some Customers weren't added to Lift and removed from Lift Queue
	 * NOT BECAUSE: (Probably means check is not working, or I need to introduce yet another check)
	 * NOT BECAUSE: (Could be because I was checking in the for loop before the index incremented, so now moved it to the top of the for loop so it checks after it's incremented)
	 * Not happening when only Jimmy used
	 * Maybe make recursive, but prob. not
	 * Probably problem with actually adding Customer – it can see ppl in Lift Queue, as size != 0 check doesn't throw on same round as NPE
	 * Happening because somehow, it thinks size of Lift Queue is 2, when it is actually 1
	 * Could be problem with Action Listener. I sorted out local/global variable issue there, but there could be another problem.
	 * Is it getting the index of the wrong Floor in finalFloors? Could be getting Ground Floor index for some reason, but more likely wrong Floor number being passed in.
	 */
	public void receiveCustomers(int floorNum) {		
		// get index of Floor Object in finalFloors array from passed in Floor number
		int fFI = 0;
		for(int i = 0; i < Shopping_Centre.finalFloors.length; i++) {
			if(Shopping_Centre.finalFloors[i].getFloorNumber() == floorNum) {
				fFI = i;
			}
		}
				
		/*
		 * checks up ahead
		 */

		// if no one in Lift Queue...
		if(Shopping_Centre.finalFloors[fFI].getLiftQueue().size() == 0) {
			// print it...
			if(currFloor != 0) {
				System.out.println("\nThere are no Customers in Floor " + floorNum + "'s Lift Queue for Lift " + name + " to pick up");
			}
			// different print statement for Ground Floor
			else {
				System.out.println("\nThere are no Customers in Ground Floor's Lift Queue for Lift " + name + " to pick up");
			}
			// and return out of method
			return;
		}
		
		// if Lift is full...
		else if(free() == false) {
			// print it...
			if(currFloor != 0) {
				System.out.println("Lift " + name + " is full, so cannot receive any Customers on Floor " + floorNum);
			}
			else {
				System.out.println("Lift " + name + " is full, so cannot receive any Customers on Ground Floor");
			}
			// and return out of method
			return;
		}
		
		// loop through occupants in Lift to look for free space to add potential Customer in lift queue to
		for(int i = 0; i < occupants.length; i++) {
			/*
			 * same checks again up ahead
			 */
			
			// if no one in Lift Queue...
			if(Shopping_Centre.finalFloors[fFI].getLiftQueue().size() == 0) {
				// print it...
				if(currFloor != 0) {
					System.out.println("There are no more Customers in Floor " + floorNum + "'s Lift Queue for Lift " + name + " to pick up");
				}
				else {
					System.out.println("There are no more Customers in Ground Floor's Lift Queue for Lift " + name + " to pick up");
				}
				// and return out of method
				return;
			}
			
			// if Lift is full...
			else if(free() == false) {
				// print it...
				if(currFloor != 0) {
					System.out.println("Lift " + name + " is now full, so cannot receive any more Customers on Floor " + floorNum);
				}
				else {
					System.out.println("Lift " + name + " is now full, so cannot receive any more Customers on Ground Floor");
				}
				// and return out of method
				return;
			}
			
			if(occupants[i] == null) { // if there is a Customer at specified index of occupants in Lift...
				occupants[i] = Shopping_Centre.finalFloors[fFI].getLiftQueue().get(0); // ...add Customer to Lift
				// if not receiving Customers on Ground Floor, do normal
				if(currFloor != 0) {
					System.out.println("Customer " + Shopping_Centre.finalFloors[fFI].getLiftQueue().get(0).getName() + " has entered Lift " + name + " on Floor " + currFloor);
				}
				// if on Ground Floor, different print statement
				else {
					System.out.println("Customer " + Shopping_Centre.finalFloors[fFI].getLiftQueue().get(0).getName() + " has entered Lift " + name + " on Ground Floor");
				}
				Shopping_Centre.finalFloors[fFI].getLiftQueue().remove(0);
			}
		}
	}
}